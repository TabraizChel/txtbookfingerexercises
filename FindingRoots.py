#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct  4 15:06:17 2018

@author: chelt
"""

def HeronsMethod(x, epsilon, power):
    guess = x/2
    counter = 0
    while abs(guess**power - x) >=  epsilon:
        guess = (guess + x/guess)/2
        print(guess)
        counter += 1
        if counter == 100:
            break
    print('attempts for herons method: ', counter)    
    return guess    
        

def Bisection(x, epsilon, power):
    counter = 0
    high = max(1.0,x)
    low = 0.0
    guess = (high + low)/2
    guess = x/2
    while abs(guess**power - x) >=  epsilon:
        if guess**power > x:
            high = guess
            
        else:
            low = guess
            
        guess = (high + low)/2
        counter += 1
        
    print('attempts for bisection search: ', counter)    
    return guess    



def NewtonRalphson(x , epsilon,power):
    guess  = x/2
    counter = 0
    while abs(guess**power - x) >=  epsilon:
        guess = guess - (guess**power - x)/(power*(guess**(power-1)))
        counter += 1
    
    print('attempts for NewtonRalphson method: ', counter)        
    return guess    

x = float(input('enter a number to check the root of: '))
epsilon = float(input('choose the accuracy to which the root should be found: '))
power = float(input('choose the exponent(order of the root): '))

rootHeron = HeronsMethod(x, epsilon, power)
rootBisection = Bisection(x, epsilon, power)
rootNewtonRalphson = NewtonRalphson(x ,epsilon,power)

print('Herons method: ', rootHeron, 'Bisection search: ',rootBisection, 'NewtonRalphson: ', rootNewtonRalphson )









            