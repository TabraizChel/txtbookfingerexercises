#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct  4 12:46:33 2018

@author: chelt
"""




def findanEven(L):
  firstEven = 0
  test = 0
  for i in L:
          if i%2 == 0:
              firstEven = i
              break
          elif i != 0:
              test += 1
  if test == len(L):
      raise ValueError('no even numbers')
      




  return firstEven       

def findanEven2(L):
  firstEven = 0
  for i in L:
      try:
           if i%2 == 0:   
              firstEven = i 
              break
      except ValueError :
          raise ValueError('no evens exist')
  



  return firstEven 


L = [1,3,1,3]
try:
    print(findanEven2(L))
except ValueError as ErrorMsg:
    print(ErrorMsg)    
