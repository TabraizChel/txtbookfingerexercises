x = 27 
epsilon = 0.01 
numGuesses = 0 
low = 0.0
high = max (1.0, x)
ans = (high + low)/2.0
while abs(abs(ans**3) - x) >=epsilon:
    print('low: ',low, 'high: ',high, 'ans:' , ans)
    numGuesses +=1
    if abs(ans**3)< x:
        low = ans
    else:
        high = ans
        
    ans = (high + low)/2.0  
    
print('guesses: ', numGuesses )   
print(ans, 'close to square root of' , x) 