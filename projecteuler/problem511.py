import itertools

# finds the potential numbers and adds them to a list (first condition)
def getPotentials(n,k):
    potential = []
    for a in range(1, n+1):
        if n%a == 0:
            potential.append(a)
            
    return potential



# takes potential numbers and generates all sequences that are correct size.
def sequence(potential , n, k):
    sequence = []
    x = itertools.combinations_with_replacement(potential, n) 
    for i  in x: 
        if (sum(i) + n)%k == 0: 
            print(i)
            y = itertools.permutations(i,n)
            for j in y:
               #print(j) 
               sequence.append(j)
               
    sequence_store = len(set(sequence))           
    print(sequence_store)
    return sequence_store





n = 4
k = 11
sequence(getPotentials(n,k) , n , k)
