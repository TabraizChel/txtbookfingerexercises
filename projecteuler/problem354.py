import math 
hex_radius = math.sqrt(3)
hex_length = 1
hex_area = 1.5*math.sqrt(3)    
start_cell = (0,0)
all_cells = []

def neighbour_count(x,y,l):
    unit = math.sqrt(3)
    
    if checker(x,y,l) == True: 
        print(x,y)
        all_cells.append((x,y))

    if len(all_cells) == 6:
        return True
       
    
    return [                      neighbour_count(x, y + unit,l),
           
            neighbour_count(x - 1.5, y + unit/2,l),                 neighbour_count(x + 1.5, y + unit/2,l),
           
            neighbour_count(x - 1.5, y - unit/2,l),                 neighbour_count(x + 1.5, y - unit/2,l),
                                  
                                  neighbour_count(x, y - unit,l)]           
    
def cell_distance(cell1, x2, y2):
    (x1,y1) = cell1
    return math.sqrt((math.fabs(x1 - x2))**2 + (math.fabs(y1 - y2))**2)


def checker(x,y, l):
    if cell_distance (start_cell, x,y) == l and (x,y) not in all_cells:
            return True
    else:
        return False
    
    
    
    
def wrapper(cell_list):    
    for cell in cell_list:
        if checker(cell, l) == True:
            all_cells.append(cell)
        return neighbour_count(cell)
    

neighbour_count(0,0,math.sqrt(3))
print(all_cells)

# =============================================================================
# for l in range(10000000):
#     wrapper(cell_list)
# =============================================================================
   
