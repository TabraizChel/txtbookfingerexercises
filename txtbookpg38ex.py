#N&T
epsilon = 0.01
k = 24.0 
guess = k/2.0
numGuessesNR = 0
while abs(guess*guess - k) >= epsilon:
    numGuessesNR += 1
    guess = guess - (((guess**2)-k)/(2*guess))
print('sqr root of ', k , 'is about', guess)    
print('Guesses for Newton Ralphson: ', numGuessesNR)
#Bisection


x = 24 
epsilon = 0.01 
numGuesses = 0 
low = 0.0
high = max (1.0, x)
ans = (high + low)/2.0
while abs(abs(ans**2) - x) >=epsilon:
    print('low: ',low, 'high: ',high, 'ans:' , ans)
    numGuesses +=1
    if abs(ans**2)< x:
        low = ans
    else:
        high = ans
        
    ans = (high + low)/2.0  
    
print('guesses bisection: ', numGuesses )   
print(ans, 'close to square root of' , x) 