#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Oct  5 22:24:30 2018

@author: chelt
"""

class IntSet(object):
    
    def __init__(self):
        
        self.vals = []
        
    def insert(self,e):
        
        if e not in self.vals:
            self.vals.append(e)
            
    def member(self, e):
        return e in self.vals
    